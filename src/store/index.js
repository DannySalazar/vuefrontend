import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex, axios)

export default new Vuex.Store({
  state: {
    profesores:[],
    materias:[],
    estudiantes:[]
  },
  mutations: {
    SET_PROFESORES(state, prof){
      state.profesores = prof
    },
    SET_ESTUDIANTES(state, est){
      state.estudiantes = est
    },
    SET_MATERIAS(state, mat){
      state.materias = mat
    }
  },
  actions: {
    obtenerProfesores({commit}){
      axios
        .get('http://localhost:3000/profesores/')
        .then(data => {
          console.log(data.data)
          let prof = data.data
          commit('SET_PROFESORES', prof)
        })
        .catch(error => {
          console.log(error)
        })
    },
    obtenerEstudiantes({commit}){
      axios
        .get('http://localhost:3000/alumnos/')
        .then(data => {
          console.log(data.data)
          let est = data.data
          commit('SET_ESTUDIANTES', est)
        })
        .catch(error => {
          console.log(error)
        })
    },
    obtenerMaterias({commit})
    {
      axios
        .get('http://localhost:3000/materias/')
        .then(data => {
          console.log(data.data)
          let mat = data.data
          commit('SET_MATERIAS', mat)
        })
        .catch(error => {
          console.log(error)
        })
    }
  },
  modules: {
    
  }
})
